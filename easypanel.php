<?php
if (!defined("WHMCS")) {
	die("This file cannot be accessed directly");
}

use Illuminate\Database\Capsule\Manager as Capsule;

function easypanel_MetaData() {
	return array(
	        'DisplayName' => 'Easypanel',
	        'APIVersion' => '1.1',
	        'RequiresServer' => true,
	    );
}

function easypanel_ConfigOptions() {
	return array(
	        'Easypanel 产品套餐类别' => array(
	            'Type' => 'dropdown',
	            'Options' => array(
	                'id' => '产品 ID',
	                'name' => '产品名',
	            ),
	        ),
	        'Easypanel 产品套餐' => array(
	            'Type' => 'text',
	            'Size' => '500',
				'Default' => '1',
	        )
	    );
}

function easypanel_CreateAccount(array $params) {
	try {
		$RandNum = epgetRandomString(4);
		$epsk = md5('add_vh'.$params['serveraccesshash'].$RandNum);
		if(empty($params['username'])) {
			$postfields['name'] = epnamegetRandomString(7);
		} else {
			$postfields['name'] = $params['username'];
		}
		$postfields['passwd'] = epgetRandomString(8);
		$postfields['init'] = '1';
		$postfields['r'] = $RandNum;
		$postfields['s'] = $epsk;
		$postfields['json'] = '1';
		if($params['configoption1'] == 'id') {
			$postfields['product_id'] = $params['configoption2'];
		} else {
			$postfields['product_name'] = $params['configoption2'];
		}
		if($params['domain']) {
			$postfields['vhost_domains'] = $params['domain'];
		}
		$ReturnInfo = json_decode(epcurlconnect('http://'.$params['serverip'].':3312/api/?c=whm&a=add_vh',$postfields),true);
		if(!$ReturnInfo) {
			throw new Exception('服务器返回信息为空');
		}
		if($ReturnInfo['result'] == 200) {
			Capsule::table('tblhosting')->where('id',$params['serviceid'])->update(['username' => $postfields['name']]);
			$adminUsername = Capsule::table('tbladmins')->first();
			if(!$adminUsername->username) {
				throw new Exception('开通失败，管理员 Username 获取失败');
			}
			$PasswdGenresults = localAPI('EncryptPassword',array('password2' => $postfields['passwd']),$adminUsername->username);
			if($PasswdGenresults['result'] != 'success') {
				throw new Exception('开通失败，产品密码加密生成失败'.json_encode($PasswdGenresults));
			} else {
				$ChangePasswd = Capsule::table('tblhosting')->where('id',$params['serviceid'])->update(['password' => $PasswdGenresults['password']]);
				if($ChangePasswd) {
					return 'success';
				} else {
					throw new Exception('开通失败，产品密码修改失败');
				}
			}
		} else {
			throw new Exception('开通失败，错误码'.$ReturnInfo['result']);
		}
	}
	catch (Exception $e) {
		logModuleCall(
		            'easypanel',
		            __FUNCTION__,
		            $params,
		            $e->getMessage(),
		            $e->getTraceAsString()
		        );
		return $e->getMessage();
	}
}

function easypanel_ChangePassword(array $params) {
	try {
		$RandNum = epgetRandomString(4);
		$epsk = md5('change_password'.$params['serveraccesshash'].$RandNum);
		$postfields['name'] = $params['username'];
		$postfields['passwd'] = $params['password'];
		$postfields['r'] = $RandNum;
		$postfields['s'] = $epsk;
		$postfields['json'] = '1';
		$ReturnInfo = json_decode(epcurlconnect('http://'.$params['serverip'].':3312/api/?c=whm&a=change_password',$postfields),true);
		if(!$ReturnInfo) {
			throw new Exception('服务器返回信息为空');
		}
		if($ReturnInfo['result'] == 200) {
			return 'success';
		} else {
			throw new Exception('密码修改失败，错误码'.$ReturnInfo['result']);
		}
	}
	catch (Exception $e) {
		logModuleCall(
		            'easypanel',
		            __FUNCTION__,
		            $params,
		            $e->getMessage(),
		            $e->getTraceAsString()
		        );
		return $e->getMessage();
	}
}

function easypanel_SuspendAccount(array $params) {
	try {
		$RandNum = epgetRandomString(4);
		$epsk = md5('update_vh'.$params['serveraccesshash'].$RandNum);
		$postfields['name'] = $params['username'];
		$postfields['status'] = '1';
		$postfields['r'] = $RandNum;
		$postfields['s'] = $epsk;
		$postfields['json'] = '1';
		$ReturnInfo = json_decode(epcurlconnect('http://'.$params['serverip'].':3312/api/?c=whm&a=update_vh',$postfields),true);
		if(!$ReturnInfo) {
			throw new Exception('服务器返回信息为空');
		}
		if($ReturnInfo['result'] == 200) {
			return 'success';
		} else {
			throw new Exception('暂停失败，错误码'.$ReturnInfo['result']);
		}
	}
	catch (Exception $e) {
		logModuleCall(
		            'easypanel',
		            __FUNCTION__,
		            $params,
		            $e->getMessage(),
		            $e->getTraceAsString()
		        );
		return $e->getMessage();
	}
}

function easypanel_UnsuspendAccount(array $params) {
	try {
		$RandNum = epgetRandomString(4);
		$epsk = md5('update_vh'.$params['serveraccesshash'].$RandNum);
		$postfields['name'] = $params['username'];
		$postfields['status'] = '0';
		$postfields['r'] = $RandNum;
		$postfields['s'] = $epsk;
		$postfields['json'] = '1';
		$ReturnInfo = json_decode(epcurlconnect('http://'.$params['serverip'].':3312/api/?c=whm&a=update_vh',$postfields),true);
		if(!$ReturnInfo) {
			throw new Exception('服务器返回信息为空');
		}
		if($ReturnInfo['result'] == 200) {
			return 'success';
		} else {
			throw new Exception('解除暂停失败，错误码'.$ReturnInfo['result']);
		}
	}
	catch (Exception $e) {
		logModuleCall(
		            'easypanel',
		            __FUNCTION__,
		            $params,
		            $e->getMessage(),
		            $e->getTraceAsString()
		        );
		return $e->getMessage();
	}
}

function easypanel_TerminateAccount(array $params) {
	try {
		$RandNum = epgetRandomString(4);
		$epsk = md5('del_vh'.$params['serveraccesshash'].$RandNum);
		$postfields['name'] = $params['username'];
		$postfields['r'] = $RandNum;
		$postfields['s'] = $epsk;
		$postfields['json'] = '1';
		$ReturnInfo = json_decode(epcurlconnect('http://'.$params['serverip'].':3312/api/?c=whm&a=del_vh',$postfields),true);
		if(!$ReturnInfo) {
			throw new Exception('服务器返回信息为空');
		}
		if($ReturnInfo['result'] == 200) {
			return 'success';
		} else {
			throw new Exception('删除失败，错误码'.$ReturnInfo['result']);
		}
	}
	catch (Exception $e) {
		logModuleCall(
		            'easypanel',
		            __FUNCTION__,
		            $params,
		            $e->getMessage(),
		            $e->getTraceAsString()
		        );
		return $e->getMessage();
	}
}

function easypanel_ClientArea(array $params) {
	try {
		$templatevar['username'] = $params['username'];
		$templatevar['password'] = $params['password'];
		$templatevar['panelurl'] = 'http://'.$params['serverip'].':3312/vhost/';
		// 配置的安全网址
		// $templatevar['panelurl'] = 'https://panel-'.strval(crc32($params['serverip'])).'.soraharu.app/vhost/';
		$templatevar['serverip'] = $params["serverip"];
		return array(
		            'tabOverviewReplacementTemplate' => 'templates/overview.tpl',
		            'templateVariables' => $templatevar,
		        );
	}
	catch (Exception $e) {
		logModuleCall(
		            'easypanel',
		            __FUNCTION__,
		            $params,
		            $e->getMessage(),
		            $e->getTraceAsString()
		        );
		return array(
		            'tabOverviewReplacementTemplate' => 'templates/error.tpl',
		            'templateVariables' => array(
		                'ErrorInfo' => $e->getMessage(),
		            ),
		        );
	}
}

function epcurlconnect($url,$postfields) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 20);
	curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
	$data = curl_exec($ch);
	curl_close($ch);
	return $data;
}

function epgetRandomString($len, $chars=null) {
	if (is_null($chars)) {
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	}
	mt_srand(10000000*(double)microtime());
	for ($i = 0, $str = '', $lc = strlen($chars)-1; $i < $len; $i++) {
		$str .= $chars[mt_rand(0, $lc)];
	}
	return $str;
}

function epnamegetRandomString($len, $chars=null) {
	if (is_null($chars)) {
		$chars = "abcdefghijklmnopqrstuvwxyz0123456789";
	}
	mt_srand(10000000*(double)microtime());
	for ($i = 0, $str = '', $lc = strlen($chars)-1; $i < $len; $i++) {
		$str .= $chars[mt_rand(0, $lc)];
	}
	return $str;
}
