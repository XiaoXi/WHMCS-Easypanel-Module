<h2>系统错误</h2>
<div class="alert alert-warning">
	<p>{$ErrorInfo}</p>
</div>
<p>请返回重试，若频繁出现此问题，请联系我们的支持人员。</p>
