{if $rawstatus eq 'active'}
<link rel="stylesheet" href="modules/servers/easypanel/theme/main.css" />
<link rel="stylesheet" href="modules/servers/easypanel/theme/flags.css" />
<div class="row m-b-15">
	<div class="col-md-6 col-sm-12">
		<h4>服务信息 <small>Service Detail</small>
		</h4>
	</div>
</div>
<div class="row easypanel-row">
	<div class="col-md-4 col-sm-12">
		<div class="box">
			<div class="boxTitle">
				产品名称
			</div>
			<div>
				<span class="boxContent">{$product}</span>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-12">
		<div class="box">
			<div class="boxTitle">
				产品状态
			</div>
			<div>
				<span class="boxContent">{$status}</span>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-12">
		<div class="box">
			<div class="boxTitle">
				到期时间
			</div>
			<div>
				<span class="boxContent">{$nextduedate}</span>
			</div>
		</div>
	</div>
</div>
<div class="row m-b-15">
	<div class="col-md-6 col-sm-12">
		<h4>产品信息 <small>Product Detail</small>
		</h4>
	</div>
</div>
<div class="row easypanel-row">
	<div class="col-md-4 col-sm-12">
		<div class="box">
			<div class="boxTitle">
				服务器 IP
			</div>
			<div>
				<span class="boxContent">{$serverip}</span>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-12">
		<div class="box">
			<div class="boxTitle">
				账户名称
			</div>
			<div>
				<span class="boxContent">{$username}</span>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-12">
		<div class="box">
			<div class="boxTitle">
				账户密码
			</div>
			<div>
				<span class="boxContent">{$password}</span>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-12">
		<a href="{$panelurl}?c=session&a=loginForm" target="_blank">
			<div class="box">
				<div class="boxTitle">
					控制面板
				</div>
				<div>
					<span class="boxContent">点击访问</span>
				</div>
			</div>
		</a>
	</div>
</div>
{else}
抱歉，该产品目前无法管理({$status})
{if $suspendreason}
，原因：{$suspendreason}
{/if}
{/if}